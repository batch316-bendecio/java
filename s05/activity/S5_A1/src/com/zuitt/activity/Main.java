package com.zuitt.activity;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();
        Contact contact1 = new Contact("John Doe", "+639152468596", "Quezon City", "Makati City", "+639228547963");


        Contact contact2 = new Contact("John Smith", "+639162148573", "Caloocan City", "Pasay City", "+639173698541");

        phonebook.addContacts(contact1);
        phonebook.addContacts(contact2);

        if(phonebook.isEmpty()){
            System.out.println("Phonebook is currently Empty!");
        } else {
            System.out.println("Contacts in Phonebook: ");
            for(Contact contact: phonebook.getContacts()){
                System.out.println(contact.getDetails());
            }
        }

    }
}
