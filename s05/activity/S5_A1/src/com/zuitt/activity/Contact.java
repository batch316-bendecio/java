package com.zuitt.activity;

import java.util.ArrayList;
import java.util.List;

public class Contact{
    private String name;
    private List<String> contactNumber;
    private List<String> address;

    public Contact(String name, String contactNumber1, String contactNumber2, String address1, String address2){
        this.name = name;
        contactNumber = new ArrayList<>();
        address = new ArrayList<>();
        contactNumber.add(contactNumber1);
        contactNumber.add(contactNumber2);
        address.add(address1);
        address.add(address2);
    }

    public String getDetails(){
        StringBuilder details = new StringBuilder();
        details.append(name).append("\n");
        details.append("---------------------------").append("\n");
        details.append(name).append(" has following registered numbers: ").append("\n");
        for(String contactNumbers : contactNumber){
            details.append(contactNumbers).append("\n");
        }
        details.append("---------------------------").append("\n");
        details.append(name).append(" has the following registered addresses: ").append("\n");
        for(String addresses : address){
            details.append(addresses).append("\n");
        }
        details.append("============================================================================");
        return details.toString();
    }
}
