package com.zuitt.example;

public class Person implements Actions, Greetings {
    public void sleep(){
        System.out.println("Zzzzzzzzz.....");
    }

    public void run(){
        System.out.println("Running on the road");
    }

    public void eat(){
        System.out.println("Nom! Nom! Nom!");
    }

    public void coding(){
        System.out.println("Google! ChatGPT! StackOverflow!");
    }

    public void holidayGreet(){
        System.out.println("Happy New Year!");
    }

    public void morningGreet(){
        System.out.println("Ohayo Gozaimasu!");
    }
}
