package com.zuitt.activity;
import java.util.HashMap;
import java.util.ArrayList;

public class PrimeNumber {
    public static void main(String[] args) {
        int[] primeNumbers = {1, 2, 3, 4, 5};

        System.out.println("The first prime number is : " + primeNumbers[1]);

        ArrayList<String> friendList = new ArrayList<String>();

        friendList.add("John");
        friendList.add("Jane");
        friendList.add("Chloe");
        friendList.add("Zoey");

        System.out.println("My friends are: " + friendList);

        HashMap<String, Integer> inventory = new HashMap<String,Integer>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);
    }
}