package com.zuitt.activity;
import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args) {
        Scanner leapYear = new Scanner(System.in);
        System.out.println("Input year to checked if a leap year");
        System.out.print("Enter year: ");
        int year = leapYear.nextInt();

        if(year % 4 == 0 && year % 100 != 0 || year % 400 == 0){
            System.out.println(year + " is a Leap Year");
        } else {
            System.out.println(year + " is NOT a Leap Year");
        }
    }
}
