package com.zuitt.example;

public class Dog extends Animal {
    //extends keyword allows us to have this class inherit the attributes and methods of the Animal class
    //Parent class is the class where we inherit from
    //Child class/subclass a class that inherits from a parent
    //No, Sub-class cannot inherit from multiple parents
    //Instead sub-class can "multiple inherit" from what we call interfaces.

    private String dogBread;

    public Dog(){
        super(); //super is a reference to the parent class. super() is us accessing the constructor method of the parent class
        this.dogBread = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.dogBread = breed;
    }

    public String getDogBread(){
        return dogBread;
    }

    public void setDogBread(String dogBread) {
        this.dogBread = dogBread;
    }

    public void greet(){
        super.call();
        System.out.println("Bark!");
    }
}
