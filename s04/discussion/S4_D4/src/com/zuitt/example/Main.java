package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car();

//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 200000;
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//
        Car car2 = new Car();
//        car2.make = "Cadillac";
//        car2.brand = "CT5-V Blackwing";
//        car2.price = 500000;
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);
//
        Car car3 = new Car();
//        car3.make = "Honda";
//        car3.brand = "Civic";
//        car3.price = 250000;
//        System.out.println(car3.brand);
//        System.out.println(car3.make);
//        System.out.println(car3.price);

        Driver driver1 = new Driver("Alejandro", 25);
//        System.out.println(driver1.name());

        car1.start();
        car2.start();
        car3.start();

        //property getters
        System.out.println(car1.getMake());
        System.out.println(car2.getMake());
        System.out.println(car3.getMake());

        //property setters
        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        car2.setMake("Cadillac");
        System.out.println(car2.getMake());

        car3.setMake("Honda");
        System.out.println(car3.getMake());

        //carDriver Getter
        System.out.println(car2.getCarDriver().getName());

        Driver newDriver = new Driver("Antonio", 21);
        car1.setCarDriver(newDriver);

        //Get name of new carDriver
        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());

        System.out.println(car1.getCarDriverName());

        Animal animal1 = new Animal("Clifford", "Red");
        animal1.call();

        Dog dog1 = new Dog();
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setName("Hachiko");
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setColor("Brown");
        System.out.println(dog1.getColor());
        dog1.greet();

        Dog dog2 = new Dog("Mike", "Dark Brown", "Corgi");
        dog2.call();
        System.out.println(dog2.getName());

        System.out.println(dog2.getDogBread());
        dog2.greet();

    }
}
