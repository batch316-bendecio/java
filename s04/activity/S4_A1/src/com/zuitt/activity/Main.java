package com.zuitt.activity;

import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        User instructor = new User("Terence Gaffud", 25, "tgaff@mail.com", "Quezon City");

        Scanner input = new Scanner(System.in);
        System.out.print("Enter Course Name: ");
        String courseName = input.nextLine();

        System.out.print("Enter Course Description: ");
        String courseDescription = input.nextLine();

        System.out.print("Enter Course Seats: ");
        int courseSeats = input.nextInt();

        System.out.print("Enter Course Fee: ");
        double courseFee = input.nextDouble();

        Course course1 = new Course();

        course1.setName(courseName);
        course1.setDescription(courseDescription);
        course1.setSeats(courseSeats);
        course1.setFee(courseFee);
        course1.setStartDate(new Date("July 10, 2023"));
        course1.setEndDate(new Date("July 14, 2023"));
        course1.setNewUser(instructor);

        System.out.println("Hi! I'm " + instructor.getName() + ". I'm " + instructor.getAge() + " years old. You can reach me via my email: " + instructor.getEmail() + ". When I'm off work, I can be found at my house in " + instructor.getAddress() + ".");

        System.out.println("---------------------------------------------------------------------------------------------------------------");

        System.out.println("Welcome to the course " + course1.getName() + ". This course can be described as " + course1.getDescription() + ". Your instructor for this course is Sir " + instructor.getName() + ". Enjoy!");
    }
}
