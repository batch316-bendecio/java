a. SELECT * FROM artist WHERE name LIKE "%d%";

b. SELECT * FROM songs WHERE length < 350;

c. SELECT album_title, song_name, length FROM albums JOIN songs ON albums.id = songs.albums_id;

d. SELECT * FROM artist JOIN albums ON artist.id = albums.artist_id WHERE album_title LIKE "%a%";

e. SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

f. SELECT * FROM albums JOIN songs ON albums.id = songs.albums_id ORDER BY album_title DESC;