package com.zuitt.activity1;
import java.util.Scanner;

public class UserDetails {
    public static void main(String[] args) {
        Scanner userDetails = new Scanner(System.in);
        System.out.print("First Name: ");
        String firstName = userDetails.nextLine();

        System.out.print("Last Name: ");
        String lastName = userDetails.nextLine();

        System.out.print("First Subject: ");
        double firstSubject = userDetails.nextDouble();

        System.out.print("Second Subject: ");
        double secondSubject = userDetails.nextDouble();

        System.out.print("Third Subject: ");
        double thirdSubject = userDetails.nextDouble();


        double average = (firstSubject + secondSubject + thirdSubject) / 3;

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + Math.round(average));
    }
}
