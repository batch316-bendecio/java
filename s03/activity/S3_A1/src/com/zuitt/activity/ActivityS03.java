package com.zuitt.activity;
import java.util.*;

public class ActivityS03 {
    public static void main(String[] args) {
        System.out.println("Input an integer whose factorial will be computed (While Loop)");
        Scanner in = new Scanner(System.in);
        System.out.print("Enter number: ");

        try {
            int num = in.nextInt();

            if (num <= 0) {
                System.out.println("Invalid number");
            } else {
                int answer = 1, counter = 1;


                //While loops
                while (counter <= num) {

                    answer *= counter;
                    counter++;

                }
                System.out.println("The factorial of " + num + " is " + answer);

                //For loops
                answer = 1;

                for (int i = 1; i <= num; i++) {
                    answer *= i;
                }
                System.out.println("The factorial of " + num + " is " + answer);
            }
        } catch (Exception e){
            System.out.println("Invalid Number");
            e.printStackTrace();
        }
    }
}
